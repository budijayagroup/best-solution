<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'best-solution');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'admin123^');

/** MySQL hostname */
define('DB_HOST', 'localhost:3306');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '!c7u)T8rQ`>|{8DLl6%$OMko&E;ce8d7y|y=:[Sz}-{<#vnY+1+AjXdbgc,.HXg ');
define('SECURE_AUTH_KEY',  '909<JT8ZTe&Z(ZW7a!$}*N/3c!!l{~=jxdypN-J%w[UGf8;Z``WAiA6*m>T.b{v!');
define('LOGGED_IN_KEY',    'e{%wYx`9zj2Vuww|9p5ANE3bX9t>C_mtZxs aK}R$!i(z^61Ov%y|kK[+n L.o$V');
define('NONCE_KEY',        ',Mw9)}_3&O;~J~:KC$lF.&uBK6lfq3H y^2HHa_Ovx]h500@bd[u7i1sO7+NHrW{');
define('AUTH_SALT',        'Y^7)^uP=m!$l@{Rbp)KY9acf8@=X~9WI+F|$cc+L./@,8!:(D[*:0|j#p+M(8AHS');
define('SECURE_AUTH_SALT', '$)&uNqpy56O8+3koj1r(V$-:{itiFASX+_2dClE+04<.uh|{X?2mywt|dBMdKV:f');
define('LOGGED_IN_SALT',   'D#Jpjrz>-K8!B9&U2nMDRs}yt#:{IWD^G(b1uP.]aU7j>Gop`qh$Ls^o8#l<d[zR');
define('NONCE_SALT',       'GHq :Iz[=6}dWPcIfTbH`lPT6EpgV!.6i!`d-o~OURnyP-b_1}}}f]sJP$[1-4E$');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_best_solution';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
